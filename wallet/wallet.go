package wallet

import (
	"errors"
	"fmt"
)

var ErrInsufficientFunds = errors.New("Oh no! Cannot wihdraw, insufficient funds!")

type Bitcoin int

type Wallet struct {
	balance Bitcoin
}

type String interface {
	String() string
}

func (w *Wallet) Deposit(amount Bitcoin) {
	w.balance += amount
}

func (w *Wallet) Balance() Bitcoin {
	return w.balance
}

func (b Bitcoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.balance {
		return ErrInsufficientFunds
	}
	w.balance -= amount
	return nil
}
