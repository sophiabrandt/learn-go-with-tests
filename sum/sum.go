package main

func Sum(numbers []int) int {
	if len(numbers) == 0 {
		return 0
	}
	sum := 0
	for _, number := range numbers {
		sum += number
	}
	return sum
}

func SumAll(collectionOfNumbers ...[]int) []int {
	var sums []int

	for _, numbers := range collectionOfNumbers {
		sums = append(sums, Sum(numbers))
	}
	return sums
}

func SumAllTails(collectionOfNumbers ...[]int) []int {
	var sums []int

	for _, numbers := range collectionOfNumbers {
		if len(numbers) == 0 {
			sums = append(sums, 0)
		} else {
			tail := numbers[1:]
			sums = append(sums, Sum(tail))
		}
	}
	return sums
}
